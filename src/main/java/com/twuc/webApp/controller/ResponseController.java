package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ResponseController {

    @GetMapping("/no-return-value")
    public void get_no_return(){

    }

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void get_no_return_with_annotation() {

    }

    @GetMapping("/messages/{message}")
    public String get_string_return(@PathVariable String message){
        return message;
    }

    @GetMapping("/message-objects/{message}")
    public Message get_object_return(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message get_object_return_with_annotation(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-entities/{message}")
    public ResponseEntity<Message> get_object_return_with_responseEntity(@PathVariable String message) {
        return ResponseEntity.ok()
                .header("X-Auth", "me")
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(message));
    }
}
