package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void get_status_without_return() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void get_status_204_no_content() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void should_get_string_message() throws Exception{
        mockMvc.perform(get("/api/messages/string-response"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("string-response"));
    }

    @Test
    void should_get_object_message()throws Exception {
        mockMvc.perform(get("/api/message-objects/object-response"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"object-response\"}"));
    }

    @Test
    void should_get_object_and_code_202() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/annotation-message"))
                .andExpect(status().is(202))
                .andExpect(content().string("{\"value\":\"annotation-message\"}"));
    }

    @Test
    void should_get_return_by_responseEntity() throws Exception{
        mockMvc.perform(get("/api/message-entities/entityResponse-message"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"value\":\"entityResponse-message\"}"))
                .andExpect(header().stringValues("X-Auth", "me"));
    }
}
